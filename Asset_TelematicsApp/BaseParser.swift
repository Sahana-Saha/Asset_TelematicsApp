//
//  BaseParser.swift
//  Asset_TelematicsApp
//
//  Created by Abhijeet Sinha on 08/04/21.
//

import UIKit

typealias VehicleConfigValues = ((Vehicle)?) -> Void

class BaseParser: NSObject {
    
    func postAction(completion : @escaping VehicleConfigValues) {
        let Url = String(format: "http://34.70.239.163/jhsmobileapi/mobile/configure/v1/configvalues")
        guard let serviceUrl = URL(string: Url) else { return }
        let parameterDictionary = ["clientid": 11,"enterprise_code": 1007, "mno": "9889897789", "passcode": 3476] as [String : Any]
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                        if let vehicle = try? decoder.decode(Vehicle.self, from:data){
                            DispatchQueue.main.async {
                                completion(vehicle)
                            }
                            print("fuelType : \(vehicle.fuelType[0].text)")
                            }
                } catch {
                    print(error)
                }
            }
        }.resume()
    }
    

}
