//
//  VehicleTypeCollectionViewCell.swift
//  Asset_TelematicsApp
//
//  Created by Abhijeet Sinha on 08/04/21.
//

import UIKit

class VehicleTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgTruck: UIImageView!
    @IBOutlet weak var btnMore_Less: UIButton!
    
    @IBOutlet weak var vehicleType_lbl: UILabel!
    
    
}
