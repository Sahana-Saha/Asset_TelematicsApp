//
//  ViewController.swift
//  Asset_TelematicsApp
//
//  Created by Abhijeet Sinha on 08/04/21.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var scrllVw: UIScrollView!
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var VehicleCollectionVw:
        UICollectionView!
    @IBOutlet weak var vwVehicleMake: UIView!
    
    @IBOutlet weak var vwVehicleModel: UIView!
    
    @IBOutlet weak var vwManufacturerYear: UIView!
    
    @IBOutlet weak var txtVehicleMake: UITextField!
    
    @IBOutlet weak var txtVehicleModel: UITextField!
    
    @IBOutlet weak var txtManufacturerYear: UITextField!
    
    @IBOutlet weak var vwFuelType: UIView!
    
    @IBOutlet weak var vwCapacity: UIView!
    
    @IBOutlet weak var txtFuelType: UITextField!
    
    @IBOutlet weak var txtCapacity: UITextField!
    
    @IBOutlet weak var vwEnterIMEI: UIView!
    @IBOutlet weak var vwIMEI: UIView!
    
    @IBOutlet weak var btnRadioOwn: UIButton!
    @IBOutlet weak var btnRadioContractor: UIButton!
    
    @IBOutlet weak var txtTagName: UITextField!
    @IBOutlet weak var txtRegisterNumber: UITextField!
    
    @IBOutlet weak var lblHeaderIMEI: UILabel!
    
    @IBOutlet weak var lblHeaderScanIMEI: UILabel!
    
    @IBOutlet weak var lblVehicleDetails: UILabel!
    
    @IBOutlet weak var lblHeaderVehicleType: UILabel!
    @IBOutlet weak var lblHeaderOwnership: UILabel!
    
    @IBOutlet weak var lblDriver: UILabel!
    
    @IBOutlet weak var vwDriver: UIView!
    
    @IBOutlet weak var imei_txt1: UITextField!
    @IBOutlet weak var imei_txt2: UITextField!
    @IBOutlet weak var imei_txt3: UITextField!
    @IBOutlet weak var imei_txt4: UITextField!
    @IBOutlet weak var imei_txt5: UITextField!
    @IBOutlet weak var imei_txt6: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    
    var vehicleDetail : Vehicle?
    var xPos : CGFloat = 0.0
    var yPos : CGFloat = 0.0
    var myWidth : CGFloat = 0.0
    var tblVwHeight : CGFloat = 0.0
    var btnTag = 0
    var isButtonMoreClicked = true
    var myView = UIView()
    var myTableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRadioOwn.isSelected = true
        btnAdd.layer.cornerRadius = 5.0
        
        vwDriver.backgroundColor = UIColor.white
        vwDriver.layer.borderColor = UIColor.darkGray.cgColor
        vwDriver.layer.borderWidth = 0.5
        vwDriver.layer.cornerRadius = 5.0
        vwDriver.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwIMEI.backgroundColor = UIColor.white
        vwIMEI.layer.borderColor = UIColor.darkGray.cgColor
        vwIMEI.layer.borderWidth = 0.5
        vwIMEI.layer.cornerRadius = 5.0
        vwIMEI.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwVehicleMake.backgroundColor = UIColor.white
        vwVehicleMake.layer.borderColor = UIColor.darkGray.cgColor
        vwVehicleMake.layer.borderWidth = 0.5
        vwVehicleMake.layer.cornerRadius = 5.0
        vwVehicleMake.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwVehicleModel.backgroundColor = UIColor.white
        vwVehicleModel.layer.borderColor = UIColor.darkGray.cgColor
        vwVehicleModel.layer.borderWidth = 0.5
        vwVehicleModel.layer.cornerRadius = 5.0
        vwVehicleModel.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwManufacturerYear.backgroundColor = UIColor.white
        vwManufacturerYear.layer.borderColor = UIColor.darkGray.cgColor
        vwManufacturerYear.layer.borderWidth = 0.5
        vwManufacturerYear.layer.cornerRadius = 5.0
        vwManufacturerYear.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwFuelType.backgroundColor = UIColor.white
        vwFuelType.layer.borderColor = UIColor.darkGray.cgColor
        vwFuelType.layer.borderWidth = 0.5
        vwFuelType.layer.cornerRadius = 5.0
        vwFuelType.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        vwCapacity.backgroundColor = UIColor.white
        vwCapacity.layer.borderColor = UIColor.darkGray.cgColor
        vwCapacity.layer.borderWidth = 0.5
        vwCapacity.layer.cornerRadius = 5.0
        vwCapacity.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        txtTagName.backgroundColor = UIColor.white
        txtTagName.layer.borderColor = UIColor.darkGray.cgColor
        txtTagName.layer.borderWidth = 0.5
        txtTagName.layer.cornerRadius = 5.0
        txtTagName.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        txtRegisterNumber.backgroundColor = UIColor.white
        txtRegisterNumber.layer.borderColor = UIColor.darkGray.cgColor
        txtRegisterNumber.layer.borderWidth = 0.5
        txtRegisterNumber.layer.cornerRadius = 5.0
        txtRegisterNumber.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt1.backgroundColor = UIColor.white
        imei_txt1.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt1.layer.borderWidth = 0.5
        imei_txt1.layer.cornerRadius = 5.0
        imei_txt1.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt2.backgroundColor = UIColor.white
        imei_txt2.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt2.layer.borderWidth = 0.5
        imei_txt2.layer.cornerRadius = 5.0
        imei_txt2.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt3.backgroundColor = UIColor.white
        imei_txt3.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt3.layer.borderWidth = 0.5
        imei_txt3.layer.cornerRadius = 5.0
        imei_txt3.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt4.backgroundColor = UIColor.white
        imei_txt4.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt4.layer.borderWidth = 0.5
        imei_txt4.layer.cornerRadius = 5.0
        imei_txt4.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt5.backgroundColor = UIColor.white
        imei_txt5.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt5.layer.borderWidth = 0.5
        imei_txt5.layer.cornerRadius = 5.0
        imei_txt5.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt6.backgroundColor = UIColor.white
        imei_txt6.layer.borderColor = UIColor.darkGray.cgColor
        imei_txt6.layer.borderWidth = 0.5
        imei_txt6.layer.cornerRadius = 5.0
        imei_txt6.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        myTableView.backgroundColor = UIColor.white
        myTableView.layer.borderColor = UIColor.darkGray.cgColor
        myTableView.layer.borderWidth = 0.5
        myTableView.layer.cornerRadius = 5.0
        myTableView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imei_txt1.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        imei_txt2.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        imei_txt3.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        imei_txt4.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        imei_txt5.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        imei_txt6.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        
        
        
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData(){
        let parser = BaseParser()
        parser.postAction { (vehicle) in
            self.vehicleDetail = vehicle
            print("vehicleType : \(self.vehicleDetail?.vehicleType)")
            self.VehicleCollectionVw.reloadData()
        }
    }
    
    
    
    func setFont(){
        lblHeaderIMEI.font = UIFont(name: "Montserrat-Medium", size: 15)
        lblHeaderScanIMEI.font = UIFont(name: "Montserrat-Medium", size: 15)
        lblVehicleDetails.font = UIFont(name: "Montserrat-Medium", size: 15)
        lblHeaderVehicleType.font = UIFont(name: "Montserrat-Medium", size: 15)
        lblHeaderOwnership.font = UIFont(name: "Montserrat-Medium", size: 15)
        lblDriver.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtVehicleMake.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtTagName.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtRegisterNumber.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtVehicleModel.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtManufacturerYear.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtFuelType.font = UIFont(name: "Montserrat-Regular", size: 15)
        txtCapacity.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt1.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt2.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt3.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt4.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt5.font = UIFont(name: "Montserrat-Regular", size: 15)
        imei_txt6.font = UIFont(name: "Montserrat-Regular", size: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if((vehicleDetail?.vehicleType.count ?? 0) != 0){
            if(isButtonMoreClicked){
                return (vehicleDetail?.vehicleType.count ?? 0) + 1
            }else{
                return 1
            }
        }else{
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //let cell: SelectSensorTableViewCell = tblPopVw.dequeueReusableCell(withIdentifier: "Cell") as! SelectSensorTableViewCell
        let cell = VehicleCollectionVw.dequeueReusableCell(withReuseIdentifier:"VwCell", for: indexPath) as? VehicleTypeCollectionViewCell
        if(isButtonMoreClicked){
            if(indexPath.row + 1 <= (vehicleDetail?.vehicleType.count ?? 0)){
                cell?.vehicleType_lbl.text = vehicleDetail?.vehicleType[indexPath.row].text
//                lblSubHeader.font = UIFont(name:"Ubuntu-Regular", size: 17)
                cell?.vehicleType_lbl.font = UIFont(name: "Montserrat-Regular", size: 15)
                cell?.imgTruck.isHidden = false
                cell?.btnMore_Less.isHidden = true
            }else{
                cell?.vehicleType_lbl.text = "More"
                cell?.imgTruck.isHidden = true
                cell?.btnMore_Less.isHidden = false
                cell?.btnMore_Less.layer.cornerRadius = 10
                cell?.btnMore_Less.setTitle("+", for: .normal)
                cell?.btnMore_Less.addTarget(self, action: #selector(btnMore_LessClicked), for: .touchUpInside)
            }
        }else{
            cell?.vehicleType_lbl.text = "Less"
            cell?.imgTruck.isHidden = true
            cell?.btnMore_Less.isHidden = false
            cell?.btnMore_Less.setTitle("-", for: .normal)
            cell?.btnMore_Less.layer.cornerRadius = 10
            cell?.btnMore_Less.addTarget(self, action: #selector(btnMore_LessClicked), for: .touchUpInside)
        }
        
        
        return cell ?? UICollectionViewCell()
    }
    
    @objc func btnMore_LessClicked(){
        if(isButtonMoreClicked){
            isButtonMoreClicked = false
            
        }else{
            isButtonMoreClicked = true
        }
        VehicleCollectionVw.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.VehicleCollectionVw.frame.size.width/4
        return CGSize(width: width , height: width)
        
    }
    
    
    func designTableVw(){
        
       // myView.frame = CGRect(x: xPos, y: yPos, width: myWidth, height: tblVwHeight)
       // myTableView.frame = self.myView.bounds
        myTableView.frame = CGRect(x: xPos, y: yPos, width: myWidth, height: tblVwHeight)
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "TblVwCell")
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.backgroundColor = .blue
      //  myView.addSubview(myTableView)
        self.vwMain.addSubview(myTableView)
        myTableView.reloadData()
        
    }
    
    
    @IBAction func btnDrpDownMakeClicked(_ sender: UIButton) {
        if(self.myTableView.isHidden){
            self.myTableView.isHidden = false
        }else{
            self.myTableView.isHidden = true
        }
       
        self.myTableView.removeFromSuperview()
        btnTag = sender.tag
        xPos  = 10
        yPos = vwVehicleMake.frame.origin.y + vwVehicleMake.frame.size.height
        myWidth = vwVehicleMake.frame.size.width
        tblVwHeight = CGFloat((vehicleDetail?.vehicleMake.count ?? 0) * 30)
        
        designTableVw()
        
    }
    
    @IBAction func btnDrpDownModelClicked(_ sender: UIButton) {
        if(self.myTableView.isHidden){
            self.myTableView.isHidden = false
        }else{
            self.myTableView.isHidden = true
        }
        self.myTableView.removeFromSuperview()
        btnTag = sender.tag
        xPos  = vwVehicleModel.frame.origin.x
        yPos = vwVehicleModel.frame.origin.y + vwVehicleModel.frame.size.height
        myWidth = vwVehicleModel.frame.size.width
        tblVwHeight = 30
        designTableVw()
        
    }
    
    @IBAction func btnDrpDownYearClicked(_ sender: UIButton) {
        if(self.myTableView.isHidden){
            self.myTableView.isHidden = false
        }else{
            self.myTableView.isHidden = true
        }
        self.myTableView.removeFromSuperview()
        btnTag = sender.tag
        xPos  = 10
        yPos = vwManufacturerYear.frame.origin.y + vwManufacturerYear.frame.size.height
        myWidth = vwManufacturerYear.frame.size.width
        tblVwHeight = CGFloat((vehicleDetail?.manufactureYear.count ?? 0) * 30)
        designTableVw()
    }
    
    @IBAction func btnDrpDownFuelTypeClicked(_ sender: UIButton) {
        if(self.myTableView.isHidden){
            self.myTableView.isHidden = false
        }else{
            self.myTableView.isHidden = true
        }
        self.myTableView.removeFromSuperview()
        btnTag = sender.tag
        xPos  = 10
        yPos = vwFuelType.frame.origin.y + vwFuelType.frame.size.height
        myWidth = vwFuelType.frame.size.width
        tblVwHeight = CGFloat((vehicleDetail?.fuelType.count ?? 0) * 30)
        designTableVw()
    }
    
    @IBAction func btnDrpDownCapacityClciked(_ sender: UIButton) {
        if(self.myTableView.isHidden){
            self.myTableView.isHidden = false
        }else{
            self.myTableView.isHidden = true
        }
        self.myTableView.removeFromSuperview()
        btnTag = sender.tag
        xPos  = 10
        yPos = vwCapacity.frame.origin.y + vwCapacity.frame.size.height
        myWidth = vwCapacity.frame.size.width
        tblVwHeight = CGFloat((vehicleDetail?.vehicleCapacity.count ?? 0) * 30)
        designTableVw()
    }
    
    
    @IBAction func btnRadioOwnershipClicked(_ sender: UIButton) {
        if(sender == btnRadioOwn){
            btnRadioOwn.isSelected = true
            btnRadioContractor.isSelected = false
        }else{
            btnRadioOwn.isSelected = false
            btnRadioContractor.isSelected = true
        }
        
    }
    
    
    @IBAction func btnScanClicked(_ sender: Any) {
        //let rvc : 
    }
    
}


extension ViewController : UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(btnTag == 500){
            return vehicleDetail?.vehicleMake.count ?? 0
        }
        else if(btnTag == 501){
            return 1
            //return vehicleDetail?..count
        }
        else if(btnTag == 502){
            return vehicleDetail?.manufactureYear.count ?? 0
        }
        else if(btnTag == 503){
            return vehicleDetail?.fuelType.count ?? 0
        }
        else{
            return vehicleDetail?.vehicleCapacity.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblVwCell", for: indexPath as IndexPath)
        if(btnTag == 500){
            cell.textLabel?.text = vehicleDetail?.vehicleMake[indexPath.row].text
        }
        else if(btnTag == 501){
            cell.textLabel?.text = "Record not found"
            //return vehicleDetail?..count
        }
        else if(btnTag == 502){
            cell.textLabel?.text = vehicleDetail?.manufactureYear[indexPath.row].text
        }
        else if(btnTag == 503){
            cell.textLabel?.text = vehicleDetail?.fuelType[indexPath.row].text
        }
        else{
            cell.textLabel?.text = vehicleDetail?.vehicleCapacity[indexPath.row].text
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(btnTag == 500){
            txtVehicleMake.text = vehicleDetail?.vehicleMake[indexPath.row].text
            self.myTableView.isHidden = true
        }
        else if(btnTag == 501){
            //cell.textLabel?.text = "Record not found"
            //return vehicleDetail?..count
            
        }
        else if(btnTag == 502){
            txtManufacturerYear.text = vehicleDetail?.manufactureYear[indexPath.row].text
            self.myTableView.isHidden = true
        }
        else if(btnTag == 503){
            txtFuelType.text = vehicleDetail?.fuelType[indexPath.row].text
            self.myTableView.isHidden = true
        }
        else{
            txtCapacity.text = vehicleDetail?.vehicleCapacity[indexPath.row].text
            self.myTableView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    @objc func textFieldDidChange(textField: UITextField){

        let text = textField.text

        if text?.utf16.count ?? 0 >= 1{
            switch textField{
            case imei_txt1:
                imei_txt2.becomeFirstResponder()
            case imei_txt2:
                imei_txt3.becomeFirstResponder()
            case imei_txt3:
                imei_txt4.becomeFirstResponder()
            case imei_txt4:
                imei_txt5.becomeFirstResponder()
            case imei_txt5:
                imei_txt6.becomeFirstResponder()
            case imei_txt6:
                imei_txt6.resignFirstResponder()
            default:
                break
            }
        }else{

        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
            textField.text = ""
        }
    
}

