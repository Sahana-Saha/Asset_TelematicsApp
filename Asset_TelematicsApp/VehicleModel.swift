//
//  VehicleModel.swift
//  Asset_TelematicsApp
//
//  Created by Abhijeet Sinha on 08/04/21.
//

import Foundation

struct Vehicle: Codable {
    let status: Int
    let message: String
    let vehicleType, vehicleCapacity, vehicleMake, manufactureYear: [FuelType]
    let fuelType: [FuelType]

    enum CodingKeys: String, CodingKey {
        case status, message
        case vehicleType = "vehicle_type"
        case vehicleCapacity = "vehicle_capacity"
        case vehicleMake = "vehicle_make"
        case manufactureYear = "manufacture_year"
        case fuelType = "fuel_type"
    }
}

// MARK: - FuelType
struct FuelType: Codable {
    let text: String
    let value: Int
    let images: String
}

